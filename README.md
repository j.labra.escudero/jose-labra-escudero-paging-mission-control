# Candidate Section:

## Installation and Execution
Steps to install & run:
1) Clone repository
2) Navigate to repository home
3) Use `npm install` to install dependencies (Jest - for Unit Tests)
4) Use `npm test` to run the unit test (uses Jest) - should see 7 tests run
5) Open `index.html` in a browser (tested with Chrome)
6) Use the file picker button to select test files (input.txt included in this project is one)
7) Results pop up in browser and also in console printout (F12)

## Assumptions

NOTE: Normally, when confusion regarding requirements and desired behaviour arises, clarity would be sought from the relevant stakeholders and/or the product owner.

Some assumptions were made during this process, in part due to unclear output requirements as they are written.
As the requirements read:

"...create alert messages for the following violation conditions:
- If for the same satellite there are three battery voltage readings that are under the red low limit within a five minute interval"

This would imply that, given the sample data, there should be 3 alert messages regarding for Satellite 1000, component TSTAT, due to warning1 (w1), warning2 (w2), warning3 (w3) all
falling within the same 5 minute interval (ie. For a message to populate with the timestamp of w1, there must exist 2 other warnings such that they are also within the same 5 minute interval - w2 & w3. Yet this implies that for w2 & w3 both, the condition also holds - that within 5 minutes of each of those timestamps there were 2 other warnings regarding the same satellite and component). Yet by looking at the sample output, it is implied that that is NOT what is desired, so we go by the example set forth in the sample output.

Since the sample output makes the implication that warnings that were used to determine when to emit an alert are NOT to generate alerts of their own and neither the sample data nor the stated requirements stated when an alert should 'end', some assumptions were made as denoted here.

1) An input file may have timestamps spanning more than a 5 minute interval.
2) A single alert message is given even if multiple events warranting it occur within 5 minutes of the alert message being given.
3) At least 5 minutes must pass before a new alert message is output, but the new alert message may take into account items that may have been used in a previous "window" (example: qualifying warning events occurr at the 2, 4, 5, and 8 minute marks. An alert message would be produced for the 2 minute timestamp event and for the 8 minute timestamp event. They both include the 4 and 5 minute mark events in their criteria).
4) Order of messages is not specified in requirements, so they have been grouped first by Satellite Id, then by Component and, within those groupings, they are sorted in order of appearance in the input file (i.e. timestamp order). Example: All satellite 1000 messages will appear before satellite 1001 messages and all of satellite 1000's TSTAT warnings will appear before satellite 1000's BATT warnings. They still appear in a single array of messages.


Original Text Follows:

# Paging Mission Control

> You are tasked with assisting satellite ground operations for an earth science mission that monitors magnetic field variations at the Earth's poles. A pair of satellites fly in tandem orbit such that at least one will have line of sight with a pole to take accurate readings. The satellite’s science instruments are sensitive to changes in temperature and must be monitored closely. Onboard thermostats take several temperature readings every minute to ensure that the precision magnetometers do not overheat. Battery systems voltage levels are also monitored to ensure that power is available to cooling coils. Design a monitoring and alert application that processes status telemetry from the satellites and generates alert messages in cases of certain limit violation scenarios.

## Requirements
Ingest status telemetry data and create alert messages for the following violation conditions:

- If for the same satellite there are three battery voltage readings that are under the red low limit within a five minute interval.
- If for the same satellite there are three thermostat readings that exceed the red high limit within a five minute interval.

### Input Format
The program is to accept a file as input. The file is an ASCII text file containing pipe delimited records.

The ingest of status telemetry data has the format:

```
<timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
```

You may assume that the input files are correctly formatted. Error handling for invalid input files may be ommitted.

### Output Format
The output will specify alert messages.  The alert messages should be valid JSON with the following properties:

```javascript
{
    "satelliteId": 1234,
    "severity": "severity",
    "component": "component",
    "timestamp": "timestamp"
}
```

The program will output to screen or console (and not to a file). 

## Sample Data
The following may be used as sample input and output datasets.

### Input

```
20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT
20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT
20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT
20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT
20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT
20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT
20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT
20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT
20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT
20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT
20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT
20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT
20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT
```

### Ouput

```javascript
[
    {
        "satelliteId": 1000,
        "severity": "RED HIGH",
        "component": "TSTAT",
        "timestamp": "2018-01-01T23:01:38.001Z"
    },
    {
        "satelliteId": 1000,
        "severity": "RED LOW",
        "component": "BATT",
        "timestamp": "2018-01-01T23:01:09.521Z"
    }
]
```
