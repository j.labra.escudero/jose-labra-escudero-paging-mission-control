/** @enum for Severity */
const Severity = {
    RedHi: "RED HIGH",
    RedLo: "RED LOW"
}

/** @enum for Component */
const Component = {
    Battery: "BATT",
    Thermostat: "TSTAT"
}

let inputFile;
let satelliteMap;

/**
 * 
 * @param {*} file File to be read
 */
function readFile(file) {
    const reader = new FileReader()
    reader.addEventListener('load', event => {
        inputFile = event.target.result;
        main();
    });

    reader.readAsText(file);
}

function main() {
    satelliteMap = parseFile(inputFile);
    outputs = [];
    satelliteMap.forEach(satellite => {
        outputs = outputs.concat(formatOutput(satellite.satelliteId, Component.Thermostat ,satellite.findThermostatWarnings()));
        outputs = outputs.concat(formatOutput(satellite.satelliteId, Component.Battery ,satellite.findBatteryWarnings()));
        
    });

    console.log(JSON.stringify(outputs, null, 4));
    document.querySelector('#output-display').textContent = JSON.stringify(outputs, null, 4);
}

/**
 * @param {string} input Multi-line string of pipe separated values
 * @returns Map of satellites, keyed by satelliteId. Each satellite contains all SatelliteNotifications belonging to it
 */
function parseFile(input) {
    let satelliteMap = new Map();
    let parsedLine;
    input = input.split('\n');
    for (let i=0; i<input.length; i++) {
        parsedLine = parseLine(input[i]);

        let currentSat = satelliteMap.has(parsedLine.satelliteId) ?
            satelliteMap.get(parsedLine.satelliteId) : new Satellite(parsedLine.satelliteId);

        let currentNotification = new SatelliteNotification(
            parsedLine.timestamp, parsedLine.redHiLim, parsedLine.yelHiLim,
            parsedLine.yelLoLim, parsedLine.redLoLim, parsedLine.value);

        currentSat.addNotification(parsedLine.component, currentNotification);
        
        satelliteMap.set(currentSat.satelliteId, currentSat);
    }
    return satelliteMap;
}

/**
 * @param {string} line Single-line string of pipe separated values
 * @returns Object containing the parsed contents of line
 */
function parseLine(line) {
    line = line.split('|');
    return {
        timestamp:  line[0], satelliteId: parseInt(line[1]),
        redHiLim: line[2], yelHiLim: line[3],
        yelLoLim: line[4], redLoLim: line[5],
        value: line[6], component: line[7].trim()
    }
}

/**
 * 
 * @param {string} satelliteId SatelliteId of satellite where warning notification occurred
 * @param {string} component Component associated with warning notifications
 * @param {SatelliteNotification[]} warningNotifications Array of warning notifications to be presented
 * @returns Array of formatted warning messages
 */
function formatOutput(satelliteId, component, warningNotifications) {
    let formattedOutput = [];
    let severityValue;
    switch(component) {
        case Component.Battery:
            severityValue = Severity.RedLo;
            break;
        case Component.Thermostat:
            severityValue = Severity.RedHi;
            break;
        default:
            console.log('Could not determine severity due to unknown component');
            break;
    }
    warningNotifications.forEach(notification => {
        formattedOutput.push({
            satelliteId: satelliteId,
            severity: severityValue,
            component: component,
            timestamp: notification.timestamp
        })
    });
    return formattedOutput;
}

class Satellite {
    batteryNotifications = [];
    thermostatNotifications = [];

    constructor(satelliteId) {this.satelliteId = satelliteId}

    /**
     * @param {string} component Component to which notification belongs to ("BATT", "TSTAT")
     * @param {SatelliteNotification} notification SatelliteNotification to be added
     */
    addNotification(component, notification) {
        switch(component) {
            case Component.Battery:
                this.batteryNotifications.push(notification);
                break;
            case Component.Thermostat:
                this.thermostatNotifications.push(notification);
                break;
            default:
                console.log("Unrecognized component");
                break;
        }
    }

    /**
     * 
     * @returns {SatelliteNotification[]} Array of SatelliteNotifications such that the battery reading has crossed the Red Low Limit
     */
    findBatteryWarnings() { 
        let exceedLimits = this.batteryNotifications.filter(notification => notification.value < notification.redLoLim);
        return this.findWarningsHelper(exceedLimits);
    }

    /**
     * 
     * @returns {SatelliteNotification[]} Array of SatelliteNotifications such that the thermostat reading has crossed the Red High Limit
     */
    findThermostatWarnings() {
        let exceedLimits = this.thermostatNotifications.filter(notification => notification.value > notification.redHiLim);
        return this.findWarningsHelper(exceedLimits);
    }

    /**
     * 
     * @param {SatelliteNotification[]} exceedLimits Array of SatelliteNotifications that have crossed some established limit
     * @returns {SatelliteNotification[]} Array of SatelliteNotifications that indicate there have been 3 or more occurrences within a 5 minute window
     */
    findWarningsHelper(exceedLimits) {
        let outputs = [];
        let warnings;
        let potentialOutput;
        for (let i=0; i<exceedLimits.length; i++) {
            warnings = exceedLimits.filter(value => {
                let timeDiff = Math.abs(Date.parse(exceedLimits[i].timestamp) - Date.parse(value.timestamp));
                return timeDiff <= 300000;
            });

            //Current warning message output must have a difference of more than 5 minutes since a message from
            //that satellite & component was presented. See "Assumptions" in README
            potentialOutput = outputs.find(output => {
                let timeDiff = Math.abs(Date.parse(exceedLimits[i].timestamp) - Date.parse(output.timestamp));
                return timeDiff <= 300000
            });

            if(warnings.length > 2 && potentialOutput === undefined) {
                outputs.push(exceedLimits[i]);
            }
        }

        return outputs;
    }
}

class SatelliteNotification {
    constructor(
        timestamp, redHiLim, yelHiLim,
        yelLoLim, redLoLim, value) {
            this.timestamp = SatelliteNotification.convertTimestamp(timestamp);
            this.redHiLim = parseFloat(redHiLim);
            this.yelHiLim = parseFloat(yelHiLim);
            this.yelLoLim = parseFloat(yelLoLim);
            this.redLoLim = parseFloat(redLoLim);
            this.value = parseFloat(value);
        }

    /**
     * 
     * @param {string} timestamp Timestamp string in "yyyymmdd hh:mm:ss.mmm" format
     * @returns {string} Timestamp string in "yyyy-mm-ddThh:mm:ss.mmmZ" format that is compatible with Date objects
     */
    static convertTimestamp(timestamp) {
        let year = timestamp.slice(0,4);
        let month = timestamp.slice(4,6);
        let day = timestamp.slice(6,8);
        let time = timestamp.split(' ')[1]
        return `${year}-${month}-${day}T${time}Z`;
    }
}


module.exports.Satellite = Satellite;
module.exports.SatelliteNotification = SatelliteNotification;
module.exports.parseFile = parseFile;
module.exports.parseLine = parseLine;
module.exports.formatOutput = formatOutput;