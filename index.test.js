const index = require('./index');

test('Parse Line to convert string to Object', () => {
    let testInput = '20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT';
    let testOutput = {
        timestamp:'20180101 23:01:05.001', satelliteId:1001,
        redHiLim: '101', yelHiLim: '98',
        yelLoLim: '25', redLoLim: '20',
        value: '99.9', component: 'TSTAT'
    }
    expect(index.parseLine(testInput)).toStrictEqual(testOutput);
    expect(index.parseLine(testInput)).not.toStrictEqual({});
});

test('Convert timestamp from "yyyymmdd hh:mm:ss.mmm" to "yyyy-mm-ddThh:mm:ss.mmmZ"', () => {
    let testInput = '20180101 23:01:05.001';
    let testOutput = '2018-01-01T23:01:05.001Z';

    expect(index.SatelliteNotification.convertTimestamp(testInput)).toStrictEqual(testOutput);
    expect(index.SatelliteNotification.convertTimestamp(testInput)).not.toStrictEqual(testInput);
});

test('Add SatelliteNotification to a Satellite', () => {
    let satellite = new index.Satellite(1000);
    let thermostatNotification = new index.SatelliteNotification('20180101 23:01:05.001','101','98','25','20','99.9');
    
    expect(satellite.thermostatNotifications.length).toBe(0);
    satellite.addNotification('TSTAT', thermostatNotification);
    expect(satellite.thermostatNotifications.length).toBe(1);
    expect(satellite.thermostatNotifications[0]).toEqual(thermostatNotification);
});

test('Parse file to convert multi-line string from file into a Map<Satellite>', () => {
    let testInput = '20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT\n' +
                    '20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT';
    let testOutput = new Map();
    let sat1 = new index.Satellite(1001);
    let sat2 = new index.Satellite(1000);
    sat1.addNotification('TSTAT', new index.SatelliteNotification('20180101 23:01:05.001','101','98','25','20','99.9'));
    sat2.addNotification('BATT', new index.SatelliteNotification('20180101 23:01:09.521','17','15','9','8','7.8'));
    testOutput.set(1001, sat1)
    testOutput.set(1000, sat2)

    expect(index.parseFile(testInput).has(1000)).toBe(true);
    expect(index.parseFile(testInput).has(1002)).toBe(false);
    expect(index.parseFile(testInput)).toEqual(testOutput);
});

test('Find SatelliteNotifications such that 3 or more warnings of battery level below red low limit in a 5 minute interval', () => {
    let testInput = '20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT\n' +
                    '20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT\n' +
                    '20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT\n' +
                    '20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT\n' +
                    '20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT\n' +
                    '20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT\n' +
                    '20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT\n' +
                    '20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT\n' +
                    '20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT\n' +
                    '20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT\n' +
                    '20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT\n' +
                    '20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT\n' +
                    '20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT\n' +
                    '20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT';

    let testOutput = [{"timestamp":"2018-01-01T23:01:09.521Z","redHiLim":17,"yelHiLim":15,"yelLoLim":9,"redLoLim":8,"value":7.8}];
    let satellite = index.parseFile(testInput).get(1000);

    expect(satellite.findBatteryWarnings().length).toBe(1);
    expect(satellite.findBatteryWarnings()).toEqual(testOutput);
    
});

test('Find SatelliteNotifications such that 3 or more warnings of thermostat temperature above red high limit in a 5 minute interval', () => {
    let testInput = '20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT\n' +
                    '20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT\n' +
                    '20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT\n' +
                    '20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT\n' +
                    '20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT\n' +
                    '20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT\n' +
                    '20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT\n' +
                    '20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT\n' +
                    '20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT\n' +
                    '20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT\n' +
                    '20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT\n' +
                    '20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT\n' +
                    '20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT\n' +
                    '20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT';
    let testOutput = [{"timestamp":"2018-01-01T23:01:38.001Z","redHiLim":101,"yelHiLim":98,"yelLoLim":25,"redLoLim":20,"value":102.9}];
    let satellite = index.parseFile(testInput).get(1000);

    expect(satellite.findThermostatWarnings().length).toBe(1);
    expect(satellite.findThermostatWarnings()).toEqual(testOutput);
});


test('Format output from warning messages into desired format', () => {
    let testInput = [{"timestamp":"2018-01-01T23:01:38.001Z","redHiLim":101,"yelHiLim":98,"yelLoLim":25,"redLoLim":20,"value":102.9}];
    let testOutput = [{"satelliteId": 1000,"severity": "RED HIGH","component": "TSTAT","timestamp": "2018-01-01T23:01:38.001Z"}];

    expect(index.formatOutput(1000, 'TSTAT', testInput)).toEqual(testOutput);
});